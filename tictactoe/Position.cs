﻿using System;

namespace TicTacToe
{
    class TicTacToePosition
    {
        private TicTacToeMarks mark;

        public TicTacToeMarks Mark
        {
            get { return mark; }
            set
            {
                // if (mark == TicTacToeMarks.X || mark == TicTacToeMarks.O)
                // {
                //     Console.WriteLine(mark);
                //     throw new ArgumentException("The position already contains a valid mark (X or O).");
                // }

                if (value != TicTacToeMarks.X && value != TicTacToeMarks.O && value != TicTacToeMarks.Empty)
                {
                    throw new ArgumentException("Invalid mark. Only X, O, or Empty are allowed.");
                }

                mark = value;
            }
        }

        public TicTacToePosition()
        {
            Mark = TicTacToeMarks.Empty;
        }

        public override string ToString()
        {
            switch (Mark)
            {
                case TicTacToeMarks.X:
                    return "X";
                case TicTacToeMarks.O:
                    return "O";
                default:
                    return "";
            }
        }
    }
}
