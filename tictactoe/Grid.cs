namespace TicTacToe
{
    enum TicTacToeMarks
    {
        X,
        O,
        Empty
    }
    class TicTacToeGrid 
    {
        private TicTacToePosition[,] grid;

        public TicTacToeGrid()
        {
            grid = new TicTacToePosition[3, 3];

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    grid[i, j] = new TicTacToePosition();
                }
            }
        }
        public TicTacToePosition[,] Grid
        {
            get { return grid; }
            set { grid = value; }
        }

        public string Winner
        {
            get
            {
                if (!string.IsNullOrEmpty(CheckRows()) || !string.IsNullOrEmpty(CheckColumns()) || !string.IsNullOrEmpty(CheckDiagonals()))
                {
                    return (CheckRows() != "") ? CheckRows() : (CheckColumns() != "") ? CheckColumns() : CheckDiagonals();
                }
                else if (CheckForTie())
                {
                    return "Tie";
                }
                else
                {
                    return "Ongoing";
                }
            }
        }

        public void PlaceCharacter(char character, int row, int column)
        {
            while (true)
            {
                if (row >= 0 && row < 3 && column >= 0 && column < 3)
                {
                    if (grid[row, column].Mark == TicTacToeMarks.Empty)
                    {
                        grid[row, column].Mark = (character == 'X') ? TicTacToeMarks.X : TicTacToeMarks.O;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("That space is taken");
                    }
                }
                else
                {
                    Console.WriteLine("That spot is invalid");
                }

                Console.WriteLine("Enter row: ");
                row = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter column:");
                column = Convert.ToInt32(Console.ReadLine());
            }
        }

        public void PrintGrid()
        {
            Console.WriteLine("-------------");
            for (int i = 0; i < 3; i++)
            {
                Console.Write("| ");
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(grid[i, j] + " | ");
                }
                Console.WriteLine();
                Console.WriteLine("-------------");
            }
        }

        public string CheckRows()
        {
            for (int row = 0; row < 3; row++)
            {
                if(grid[row,0].Mark == grid[row,1].Mark && grid[row,1].Mark == grid[row,2].Mark)
                {
                    if(grid[row,0].Mark != TicTacToeMarks.Empty)
                    {
                        return grid[row,0].Mark.ToString();
                    }
                }
            }
            return "";
        }

        public string CheckColumns() 
        {
            for(int column = 0; column < 3; column++)
            {
                if(grid[0,column].Mark == grid[1,column].Mark && grid[1,column].Mark == grid[2, column].Mark)
                {
                    if(grid[0,column].Mark != TicTacToeMarks.Empty)
                    {
                        return grid[0,column].Mark.ToString();
                    }
                }
            }
            return "";
        }

        public string CheckDiagonals()
        {
            if (grid[0, 0].Mark == grid[1, 1].Mark && grid[1, 1].Mark == grid[2, 2].Mark && grid[0,0].Mark != TicTacToeMarks.Empty)
            {
                return grid[0, 0].Mark.ToString();
            }

            if (grid[0, 2].Mark == grid[1, 1].Mark && grid[1, 1].Mark == grid[2, 0].Mark && grid[0, 2].Mark != TicTacToeMarks.Empty)
            {
                return grid[0, 2].Mark.ToString();
            }

            return "";
        }

        public bool CheckForTie()
        {
            foreach (TicTacToePosition position in grid)
            {
                if (position.Mark == TicTacToeMarks.Empty)
                {
                    return false;
                }
            }
            return true;
        }

        public static void Main(string[] args) 
        {
            TicTacToeGrid game = new TicTacToeGrid();
            game.PrintGrid();

            char currentPlayer = 'X'; 

            while(true)
            {
                Console.WriteLine($"Player {currentPlayer}'s turn");

                Console.WriteLine("Enter row: ");
                int row = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter column:");
                int column = Convert.ToInt32(Console.ReadLine());

                game.PlaceCharacter(currentPlayer, row, column);
                game.PrintGrid();

                if(game.Winner == "Tie")
                {
                    Console.WriteLine("It's a tie!!!");
                    break;
                }
                if(game.Winner == "Ongoing")
                {
                    Console.WriteLine("Game is ongoing!");
                }
                if (game.Winner == "X" || game.Winner == "O")
                {
                    Console.WriteLine($"Player {game.Winner} wins!");
                    break;
                }    
                currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
            }
        }
    }
}
